Source: pygopherd
Section: net
Priority: optional
Maintainer: John Goerzen <jgoerzen@complete.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 docbook-utils,
 ghostscript,
 python3-all,
 python3-legacy-cgi,
 python3-setuptools,
 python3-zombie-imp,
#Testsuite: autopkgtest-pkg-python
Standards-Version: 4.6.1
Homepage: https://github.com/michael-lazar/pygopherd
Vcs-Browser: https://salsa.debian.org/python-team/packages/pygopherd
Vcs-Git: https://salsa.debian.org/python-team/packages/pygopherd.git

Package: pygopherd
Architecture: all
Depends:
 adduser,
 media-types,
 logrotate,
 python3-legacy-cgi,
 python3-simpletal,
 python3-zombie-imp,
 ${python3:Depends},
 ${misc:Depends},
Provides: gopher-server
Description: Modular Multiprotocol Gopher/HTTP/WAP Server
 This is a modern Gopher server.  It can serve documents
 with Gopher+, standard Gopher (RFC1436), HTTP, and WAP -- all on the same
 port.  Pygopherd features a modular extension system as well as
 loadable scripts and much more.  It contains full support for
 UMN gopherd systems -- including .Links, .names, .cap, searches, etc.
 Pygopherd also supports Bucktooth features such as gophermap files
 and executables.  In addition to all this, there are Pygopherd's own
 extra features.  All features are fully customizable and can be enabled
 or disabled by editing /etc/pygopherd/pygopherd.conf.
