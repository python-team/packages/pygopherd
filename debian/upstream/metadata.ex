# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/pygopherd/issues
# Bug-Submit: https://github.com/<user>/pygopherd/issues/new
# Changelog: https://github.com/<user>/pygopherd/blob/master/CHANGES
# Documentation: https://github.com/<user>/pygopherd/wiki
# Repository-Browse: https://github.com/<user>/pygopherd
# Repository: https://github.com/<user>/pygopherd.git
